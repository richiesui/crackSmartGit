# 仅供学习研究，请支持正版


# 关键验证

搜索字符串常量 ``Signature``或``Format=``或查找``MessageDigest``类型引用，定位到具体类,
一般只会搜索到一个。

#### 18.2.9
![img_1.png](img_1.png)

#### 21.2.4
![img.png](img.png)

## 从版本22.1开始，关键验证函数原型产生变化,函数形参列表从2个变为3个(新增int paramInt参数,以及可见性从public变更为private),见图
![img_2.png](img_2.png)
所以原始patch将无法正确匹配